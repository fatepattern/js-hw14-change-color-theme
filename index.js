let changeThemeButton = document.querySelector(".theme-button");
let topMenu = document.querySelector(".top-menu");

function setTheme(theme){
    document.body.className = theme;
}

function changeColorsForLightTheme(){
    document.body.style.background = "white";
    changeThemeButton.style.backgroundColor = "rgb(17, 81, 141)";
    topMenu.style.backgroundColor = "#35444F";
}

function changeColorsForDarkTheme(){
    document.body.style.background = "brown";
    changeThemeButton.style.backgroundColor = "brown";
    topMenu.style.backgroundColor = "red";
}

if(localStorage.getItem('theme') === 'dark'){
    changeColorsForDarkTheme();
} else if(localStorage.getItem('theme') === 'light'){
    changeColorsForLightTheme();
}

let appliedTheme = localStorage.getItem('theme');

if(appliedTheme){
    setTheme(appliedTheme);
} else {
    localStorage.setItem('theme', 'light');
    setTheme('light');
}

changeThemeButton.addEventListener("click", function(){
    if(localStorage.getItem('theme') === 'light'){
        localStorage.setItem('theme', 'dark');
        setTheme('dark');
        changeColorsForDarkTheme();
    } else {
        localStorage.setItem('theme', 'light');
        setTheme('light');
        changeColorsForLightTheme();
    }
})


